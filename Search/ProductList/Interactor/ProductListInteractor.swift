import Foundation


// MARK: *** ProductListInteractorProtocol
class ProductListInteractor: ProductListInteractorProtocol {
  var dataManager: ProductListDataManagerProtocol?
  var presenter: ProductListPresenterProtocol?
  
  func retrieveProducts() {
    dataManager?.retrieveProductList()
  }
}

// MARK: *** ProductListDataManagerCallbackProtocol
extension ProductListInteractor: ProductListDataManagerCallbackProtocol {
  func onRetrieveProducts(_ interactor: ProductListInteractorProtocol, products: [Product]) {
    if let asyncPresenter = interactor.presenter as? ProductListInteractorCallbackProtocol {
      asyncPresenter.didRetrieveProducts(interactor.presenter!, products: products)
    }
  }
  
  func onError(_ interactor: ProductListInteractorProtocol){
    if let asyncPresenter = interactor.presenter as? ProductListInteractorCallbackProtocol {
      asyncPresenter.didRetrieveProducts(interactor.presenter!, products: [])
    }
  }
}
