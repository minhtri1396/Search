import Foundation

class CommentParser: ProductListDataManagerParserProtocol {
  class func parse(data: [String : AnyObject]) -> [AnyObject] {
    var commentList: [Comment] = [Comment]()
    guard let comments = data["comments"] as? [[String: AnyObject]] else { return commentList }
    
    for comment in comments {
      let commentDetail = Comment(
        id: UInt64("\(comment["id"]!)")!,
        content: comment["content"] as! String
      )
      commentList.append(commentDetail)
    }
    
    return commentList
  }
}
