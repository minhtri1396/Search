import Foundation

class GiftParser: ProductListDataManagerParserProtocol {
  class func parse(data: [String : AnyObject]) -> [AnyObject] {
    var giftList: [Gift] = [Gift]()
    guard let gifts = data["gifts"] as? [[String: AnyObject]] else { return giftList }
    
    for gift in gifts {
      let giftDetail = Gift(
        id: UInt64("\(gift["id"]!)")!,
        name: gift["name"] as! String,
        manu: gift["manu"] as! String,
        price: gift["price"] as! Double
      )
      giftList.append(giftDetail)
    }
    
    return giftList
  }
}
