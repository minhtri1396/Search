import UIKit


// MARK: *** ProductListRouterProtocol
class ProductListRouter: ProductListRouterProtocol {
  class func createProductListModule() -> UIViewController {
    let viewController = mainStoryboard.instantiateViewController(withIdentifier: "TabBarController")
    if let view = viewController.childViewControllers[0].childViewControllers[0] as? ProductCollectionView { // tab bar -> navigation bar -> ProductCollectionView
      let presenter: ProductListPresenterProtocol = ProductListPresenter()
      let router: ProductListRouterProtocol = ProductListRouter()
      let interator: ProductListInteractorProtocol = ProductListInteractor()
      let dataManager: ProductListDataManagerProtocol = ProductListDataManager()
      
      view.presenter = presenter
      presenter.view = view
      presenter.router = router
      presenter.interactor = interator
      interator.presenter = presenter
      interator.dataManager = dataManager
      dataManager.requester = interator
      
      return viewController
    }
    
    return UIViewController()
  }
  
  static var mainStoryboard: UIStoryboard {
    return UIStoryboard(name: "Main", bundle: Bundle.main)
  }
  
  func presentProductDetailScreen(view: ProductCollectionViewProtocol, product: Product){
    let productDetailViewController = ProductDetailRouter.createProductDetailModule(forProduct: product)
    
    if let sourceView = view as? UIViewController {
      sourceView.navigationController?.pushViewController(productDetailViewController, animated: true)
    }
  }
}
