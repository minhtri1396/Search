import Foundation

class NumberHelper {
  class func addDot(forNumber number: Double, withNDecs nDecs: UInt8) -> String {
    let rounded = round(forNumber: number, withNDecs: nDecs)
    let intNumber = Int64(rounded)
    let decimal = rounded.adding(Double(-intNumber))
    
    let decimalAsStr = "\(decimal)"
    var result = formattedWithSeparator(number: intNumber, separator: ".")
    if decimal > 0 {
      let range: Range<String.Index> = decimalAsStr.range(of: ".")!
      result += ",\(decimalAsStr.substring(from: range.lowerBound))"
    }
    return result
  }
  
  class func round(forNumber number: Double, withNDecs nDecs: UInt8) -> Double {
    if nDecs == 0 {
      return Double(Int64(number))
    }
    
    let intNumber = Int64(number.multiplied(by: Double(nDecs * 10)))
    return Double(intNumber / Int64(nDecs * 10))
  }
  
  class func formattedWithSeparator(number: Int64, separator: String) -> String {
    let formatter = NumberFormatter()
    formatter.groupingSeparator = separator
    formatter.numberStyle = .decimal
    return formatter.string(from: NSNumber(value: Int64(number))) ?? ""
  }
}
