import Foundation

class Gift {
  let id: UInt64
  var manu: String // manufactory
  var name: String
  var price: Double
  
  init(id: UInt64, name: String, manu: String, price: Double) {
    self.id = id
    self.name = name
    self.manu = manu
    self.price = price
  }
}
