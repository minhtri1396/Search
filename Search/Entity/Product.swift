import UIKit

class Product {
  let id: UInt64
  var comments: [Comment]
  var gifts: [Gift]
  var images: [UIImage]
  var manu: String          // manufactory
  var name: String
  var numOfLike: UInt32
  var perDiscount: Double   // percent discount
  var provider: String
  var realPrice: Double
  var salePrice: Double
  var isAvailable: Bool
  
  init(id: UInt64, name: String, provider: String, manu: String, isAvailable: Bool) {
    self.id = id
    comments = [Comment]()
    gifts = [Gift]()
    images = [UIImage]()
    self.isAvailable = isAvailable
    self.manu = manu
    self.name = name
    numOfLike = 0
    perDiscount = 0
    self.provider = provider
    realPrice = 0
    salePrice = 0
  }
}
