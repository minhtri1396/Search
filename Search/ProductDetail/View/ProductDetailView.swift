import UIKit
import ImageSlideshow


// MARK: *** UIViewController
class ProductDetailView: UIViewController {
  @IBOutlet weak var providerLabel: UILabel!
  @IBOutlet weak var manuLabel: UILabel!
  @IBOutlet weak var priceLabel: UILabel!
  @IBOutlet weak var numLikeLabel: UILabel!     // the number of like
  @IBOutlet weak var stateLabel: UILabel!       // available or not
  @IBOutlet weak var nameLabel: UILabel!
  @IBOutlet weak var productImagesView: UIView!
  
  var presenter: ProductDetailPresenterProtocol?
  
  @IBAction func addingButtonTapped(_ button: Any) {
    
  }
  
  func commentGestureTapped(_ gesture: Any) {
    
  }
  
  override func viewDidLoad() {
    super.viewDidLoad()
    presenter?.viewDidLoad()
    
    let commentGesture = UITapGestureRecognizer(target: self, action: #selector(commentGestureTapped(_:)))
    commentGesture.numberOfTapsRequired = 1
    
    let rightLoveButtonItem:UIBarButtonItem = UIBarButtonItem(image: UIImage(named: "icon_nav_love"), style: UIBarButtonItemStyle.plain, target: self, action: nil)
    let rightShareButtonItem:UIBarButtonItem = UIBarButtonItem(image: UIImage(named: "icon_nav_share"), style: UIBarButtonItemStyle.plain, target: self, action: nil)
//    let rightCartButtonItem:UIBarButtonItem = UIBarButtonItem(image: UIImage(named: "icon_nav_cart"), style: UIBarButtonItemStyle.plain, target: self, action: nil)
    
    // Test UI
    let cartBarbuttonItem = MIBadgeButton(frame: CGRect(x: 40, y: 5, width: 40, height: 44))
    _ = cartBarbuttonItem.initWithFrame(frame: cartBarbuttonItem.frame, withBadgeString: "7", withBadgeInsets:  UIEdgeInsetsMake(20, 2, 0, 10))
    cartBarbuttonItem.badgeBackgroundColor = UIColor(red: 255/255, green: 123/255, blue: 45/255, alpha: 1)
    cartBarbuttonItem.setImage(UIImage(named: "icon_nav_cart"), for: .normal)
    cartBarbuttonItem.setImage(UIImage(named: "icon_nav_cart"), for: .selected)
//    cartBarbuttonItem?.addTarget(self, action: Selector("loadCart"), for: .touchUpInside)
    
    navigationItem.setRightBarButtonItems([UIBarButtonItem(customView: cartBarbuttonItem), rightShareButtonItem, rightLoveButtonItem], animated: true)
  }
  
  override func viewWillAppear(_ animated: Bool) {
    navigationController?.navigationBar.barTintColor = UIColor.white
  }
}

// MARK: *** ProductDetailViewProtocol
extension ProductDetailView: ProductDetailViewProtocol {
  func showProduct(forProduct product: Product) {
    priceLabel.text = "\(NumberHelper.addDot(forNumber: product.salePrice, withNDecs: 0))"
    providerLabel.text = "\(product.provider)"
    manuLabel.text = "\(product.manu)"
    numLikeLabel.text = "\(product.numOfLike) Yêu thích"
    nameLabel.text = product.name
    
    if product.isAvailable {
      stateLabel.text = "Còn hàng"
    } else {
      stateLabel.text = "Hết hàng"
      stateLabel.textColor = UIColor.red
    }
    
    if product.images.count > 0 {
      let imageSlideshow = ImageSlideshow(frame: CGRect(x: 0, y: 0, width: productImagesView.bounds.width, height: productImagesView.bounds.height))
      imageSlideshow.pageControlPosition = PageControlPosition.underScrollView
      imageSlideshow.pageControl.currentPageIndicatorTintColor = UIColor.lightGray
      imageSlideshow.pageControl.pageIndicatorTintColor = UIColor.black
      
      var imageSources = [ImageSource]()
      for image in product.images {
        imageSources.append(ImageSource(image: image))
      }
      imageSlideshow.setImageInputs(imageSources)
      productImagesView.addSubview(imageSlideshow)
    }
  }
}
