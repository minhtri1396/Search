import UIKit


//  MARK: *** PRESENTER
protocol ProductDetailPresenterProtocol: class {
  var view: ProductDetailViewProtocol? {get set}
  var router: ProductDetailRouterProtocol? {get set}
  var product: Product! {get set}
  
  // VIEW -> PRESENTER
  func viewDidLoad()            // show the product (detail) via view
}

//  MARK: *** ROUTER
protocol ProductDetailRouterProtocol: class {
  static func createProductDetailModule(forProduct product: Product) -> UIViewController
}

//  MARK: *** VIEW
protocol ProductDetailViewProtocol: class {
  var presenter: ProductDetailPresenterProtocol? {get set}
  
  // PRESENTER -> VIEW
  func showProduct(forProduct product: Product)
}
